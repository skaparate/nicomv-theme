<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package nicomv
 */

get_header(); ?>

<section id="primary" class="content-area">
	<main id="main" class="site-main">
		<?php if ( have_posts() ) : ?>
		<header class="page-header columns is-vcentered is-mobile">
			<h2 class="page-title is-offset-1 is-5-tablet is-three-quarters-mobile column">
			<?php
			/* translators: %s: search query. */
			printf( esc_html__( 'Search Results for: %s', 'nicomv' ), '<span>' . get_search_query() . '</span>' );
			?>
			</h2>
		</header><!-- .page-header -->
		<div class="columns is-mobile">
			<div class="column is-offset-1 is-2-tablet is-three-quarters-mobile">
			<?php get_search_form(); ?>
			</div>
		</div>
		<div class="columns is-mobile">
			<div class="column is-10-tablet is-offset-1">
				<div class="grid">
					<?php
					/* Start the Loop */
					while ( have_posts() ) :
						the_post();

						/**
						 * Run the loop for the search to output the results.
						 * If you want to overload this in a child theme then include a file
						 * called content-search.php and that will be used instead.
						 */
						get_template_part( 'template-parts/content', 'search' );

					endwhile;
					?>
				</div>
					<?php
					the_posts_navigation();

				else :
					get_template_part( 'template-parts/content', 'none' );
				endif;
				?>
			</div><!-- .column -->
		</div><!-- .columns -->
	</main><!-- #main -->
</section><!-- #primary -->
<?php
get_footer();
