<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nicomv
 */

$body_classes = [];
$body_classes[] = nicomv_is_sticky_menu() ? 'fixed-top-nav' : '';
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class( $body_classes ); ?>>
<!-- Anchor for the "go to top" button -->
<div id="page" class="site">
	<a class="skip-link screen-reader-text sr-only" href="#content">
		<?php esc_html_e( 'Skip to content', 'nicomv' ); ?> <i class="icon-hand-o-down"></i>
	</a>

	<header id="masthead" class="site-header">
		<nav id="site-navigation" class="site-header-navigation main-navigation navbar light" role="navigation" aria-label="main navigation">
			<div class="navbar-branding site-branding navbar-brand">
				<?php if ( has_custom_logo() ) : ?>
				<?php the_custom_logo(); ?>
				<?php else : ?>
					<figure>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
							<img src="<?php echo esc_html( NICOMV_THEME_URI . '/assets/images/logo-white.svg' ); ?>" title="NicoMV Logo" alt="<?php esc_html_e( 'NicoMV Logo', 'nicomv' ); ?>" />
						</a>
					</figure>
				<?php endif ?>
				<h1 class="site-title sr-only"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
				$description = get_bloginfo( 'description', 'display' );
				if ( $description || is_customize_preview() ) :
				?>
					<p class="site-description sr-only"><?php echo $description; /* WPCS: xss ok. */ ?></p>
				<?php endif; ?>
				<button class="button navbar-burger burger" aria-controls="mainNav" aria-expanded="false" data-target="mainNav">
					<span class="sr-only"><?php esc_html_e( 'Show/Hide Menu', 'nicomv' ); ?></span>
					<i class="icon-menu"></i>
				</button>
			</div><!-- .site-branding -->
			<div id="mainNav" class="navbar-menu">
				<div class="navbar-center">
					<?php
						$_menu_options = array(
							'theme_location'    => '',
							'menu_id'           => '',
							'container'         => null,
							'walker'            => new Nicomv_BulmaMenuWalker(),
							'items_wrap'        => '%3$s',
						);
						if ( has_nav_menu( 'frontpage-menu' ) ) :
							$_menu_options['theme_location'] = 'frontpage-menu';
							$_menu_options['menu_id']        = 'frontpage-menu';
							wp_nav_menu( $_menu_options );
						elseif ( has_nav_menu( 'blog-menu' ) ) :
							$_menu_options['theme_location'] = 'blog-menu';
							$_menu_options['menu_id']        = 'blog-menu';
							wp_nav_menu( $_menu_options );
						endif;
						?>
				</div>
				<div class="navbar-end">
						<div class="social-navigation social-header" aria-label="<?php esc_html_e( 'Social', 'nicomv' ); ?>">
							<h4 class="is-hidden-desktop"><?php esc_html_e( 'Social', 'nicomv' ); ?></h4>
							<?php
							if ( has_nav_menu( 'social-menu-header' ) ) :
								$_menu_options['theme_location'] = 'social-menu-header';
								$_menu_options['menu_id']        = 'social-menu-header';
								$_menu_options['walker']         = new Nicomv_SocialMenuWalker();
								wp_nav_menu( $_menu_options );
							endif;
							?>
						</div>
						<?php
						if ( has_nav_menu( 'language' ) ) :
							$_menu_options['theme_location']    = 'language';
							$_menu_options['menu_id']           = 'language';
							$_menu_options['walker']            = new Nicomv_BulmaMenuWalker();
							$_menu_options['items_wrap']        = '<div id="%1$s" class="navbar-item has-dropdown is-hoverable lang-menu %2$s">%3$s</div>';
							wp_nav_menu( $_menu_options );
						endif;
					?>
					<?php
					if ( nicomv_use_navigation_search_bar() ) :
					?>
					<div class="navbar-item search-container">
						<?php get_search_form(); ?>
					</div>
					<?php
					endif;
					?>
				</div>
			</div>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
