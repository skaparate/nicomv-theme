# Copyright (C) 2017 Automattic
# This file is distributed under the GNU General Public License v2 or later.
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: NicoMV Theme\n"
"Report-Msgid-Bugs-To: https://wordpress.org/tags/_s\n"
"POT-Creation-Date: 2019-10-02 15:58-0300\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2017-MO-DA HO:MI+ZONE\n"
"Last-Translator: \n"
"Language-Team: Nicolas Mancilla Vergara\n"
"X-Generator: Poedit 2.2.1\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-KeywordsList: __;_e;_n;_x;_ex;_nx;esc_attr__;esc_attr_e;esc_attr_x;"
"esc_html__;esc_html_e;esc_html_x;_n_noop;_nx_noop;translate_nooped_plural\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: assets\n"
"X-Poedit-SearchPathExcluded-1: _dev\n"
"X-Poedit-SearchPathExcluded-2: vendor\n"

#: 404.php:17
msgid "Oops! That page can&rsquo;t be found."
msgstr ""

#: 404.php:22
msgid ""
"It looks like nothing was found at this location. Maybe try one of the links "
"below or a search?"
msgstr ""

#: comments.php:39
#, php-format
msgid "One thought on &ldquo;%1$s&rdquo;"
msgstr ""

#: comments.php:45
#, php-format
msgid "%1$s thought on &ldquo;%2$s&rdquo;"
msgstr ""

#: comments.php:72
msgid "Comments are closed."
msgstr ""

#: footer.php:28 header.php:78 header.php:79
msgid "Social"
msgstr ""

#: footer.php:49
#, php-format
msgid "All rights reserved %s"
msgstr ""

#: footer.php:61
msgid "Language"
msgstr ""

#: footer.php:80
msgid "Go to the Top"
msgstr ""

#: functions.php:92
msgid "Small"
msgstr ""

#: functions.php:93
msgid "S"
msgstr ""

#: functions.php:98
msgid "Normal"
msgstr ""

#: functions.php:99
msgid "M"
msgstr ""

#: functions.php:104
msgid "Large"
msgstr ""

#: functions.php:105
msgid "L"
msgstr ""

#: functions.php:110
msgid "Huge"
msgstr ""

#: functions.php:111
msgid "XL"
msgstr ""

#: functions.php:154 template-parts/content.php:10
msgid "Projects"
msgstr ""

#: functions.php:167
msgid "Sidebar"
msgstr ""

#: functions.php:169
msgid "Add widgets here"
msgstr ""

#: functions.php:179
msgid "Footer - Main Widget Area"
msgstr ""

#: functions.php:181
msgid "Add widgets to the footer"
msgstr ""

#: header.php:29
msgid "Skip to content"
msgstr ""

#: header.php:40
msgid "NicoMV Logo"
msgstr ""

#: header.php:52
msgid "Show/Hide Menu"
msgstr ""

#: inc/class-nicomv-themecustomizer.php:37 inc/customizer.php:38
msgid "Theme specific settings"
msgstr ""

#: inc/class-nicomv-themecustomizer.php:96 inc/customizer.php:47
msgid "Sticky main navigation"
msgstr ""

#: inc/class-nicomv-themecustomizer.php:97 inc/customizer.php:48
msgid "Used to specify if the main navigation should stick to the top"
msgstr ""

#: inc/class-nicomv-themecustomizer.php:100
#: inc/class-nicomv-themecustomizer.php:123
#: inc/class-nicomv-themecustomizer.php:146 inc/customizer.php:51
msgid "Yes"
msgstr ""

#: inc/class-nicomv-themecustomizer.php:101
#: inc/class-nicomv-themecustomizer.php:124
#: inc/class-nicomv-themecustomizer.php:147 inc/customizer.php:52
msgid "No"
msgstr ""

#: inc/class-nicomv-themecustomizer.php:119
msgid "Navigation Search Bar"
msgstr ""

#: inc/class-nicomv-themecustomizer.php:120
msgid "Specifies if a search form should be attached to the main navigation"
msgstr ""

#: inc/class-nicomv-themecustomizer.php:142
msgid "Disable Comments"
msgstr ""

#: inc/class-nicomv-themecustomizer.php:143
msgid "Disable comments globally"
msgstr ""

#: inc/menus-setup.php:16
msgid "Blog Menu"
msgstr ""

#: inc/menus-setup.php:17
msgid "Front page menu"
msgstr ""

#: inc/menus-setup.php:18
msgid "Language Menu"
msgstr ""

#: inc/menus-setup.php:19
msgid "Header Social Menu"
msgstr ""

#: inc/menus-setup.php:20
msgid "Footer Social Menu"
msgstr ""

#: inc/menus-setup.php:21
msgid "Footer Language Menu"
msgstr ""

#: inc/template-tags.php:42
#, php-format
msgid "Leave a Comment<span class=\"screen-reader-text\"> on %s</span>"
msgstr ""

#: inc/template-tags.php:71
msgid "Edit Entry"
msgstr ""

#: inc/template-tags.php:95
#, php-format
msgid "Tag %s"
msgstr ""

#: search.php:19
#, php-format
msgid "Search Results for: %s"
msgstr ""

#: searchform.php:10
msgid "Search"
msgstr ""

#: searchform.php:11
msgid "I'm looking for..."
msgstr ""

#: searchform.php:12
msgid "Begins the Search"
msgstr ""

#: searchform.php:13
msgid "Begin the search"
msgstr ""

#: template-parts/content-cookie-warning.php:10
msgid "Like many sites, we too use cookies to enhance the user experience."
msgstr ""

#: template-parts/content-cookie-warning.php:12
msgid "Accept"
msgstr ""

#: template-parts/content-none.php:13
msgid "Nothing Found"
msgstr ""

#: template-parts/content-none.php:25
#, php-format
msgid "Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#: template-parts/content-none.php:42
msgid ""
"Sorry, but nothing matched your search terms. Please try again with some "
"different keywords."
msgstr ""

#: template-parts/content-none.php:56
msgid ""
"It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching "
"can help."
msgstr ""

#: template-parts/content-page.php:23 template-parts/content.php:59
msgid "Pages:"
msgstr ""

#: template-parts/content-page.php:37
#, php-format
msgid "Edit <span class=\"screen-reader-text\">%s</span>"
msgstr ""

#: template-parts/content-tags.php:12
msgid "Tags"
msgstr ""

#: template-parts/content.php:47
#, php-format
msgid "Continue reading <span class=\"screen-reader-text\">\"%s\"</span>"
msgstr ""

#: template-parts/meta-category.php:12
msgid "Categories this post is published on"
msgstr ""

#: template-parts/meta-posted-on.php:13
msgid "Published on"
msgstr ""

#: template-parts/meta-posted-on.php:20
msgid "Last update:"
msgstr ""

#: template-parts/pages/page-blog.php:29
#, php-format
msgid "Continue reading \"%s\""
msgstr ""

#: template-parts/pages/page-front.php:37
#, php-format
msgid "Continue reading<span class=\"screen-reader-text\"> \"%s\"</span>"
msgstr ""

#: template-parts/post-navigation.php:13
msgid "Recommended"
msgstr ""
