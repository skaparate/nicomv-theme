<?php
/**
 * The home page format.
 *
 * @package nicomv
 */

	get_header();
?>

<div id="blog-content" class="columns">
	<div class="column is-7 is-offset-1">
	<?php
	$query = nicomv_blog_posts();
	if ( $query->have_posts() ) :
		while ( $query->have_posts() ) :
			$query->the_post();
			get_template_part( 'template-parts/pages/page', 'blog' );
		endwhile;
		wp_reset_postdata();
	endif;
	?>
	</div>
	<div class="column is-3">
		<?php get_sidebar(); ?>
	</div>
</div>

<?php
	get_footer();
?>
