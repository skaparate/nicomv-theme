<?php
/**
 * Search form template.
 *
 * @package nicomv
 */

?>
<form class="search-form" role="search" method="get" id="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label class="sr-only search-form--label" for="search-input"><?php echo esc_html__( 'Search', 'nicomv' ); ?></label>
	<input class="search-form--input" type="search" placeholder="<?php echo esc_html__( 'I\'m looking for...', 'nicomv' ); ?>" name="s" id="search-input" value="<?php echo esc_attr( get_search_query() ); ?>" />
	<button type="submit" class="button search-form--submit" id="search-submit" title="<?php echo esc_html__( 'Begins the Search', 'nicomv' ); ?>">
		<span class="sr-only"><?php esc_html__( 'Begin the search', 'nicomv' ); ?></span>
		<i class="icon-search"></i>
	</button>
</form>
