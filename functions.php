<?php
/**
 * Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package nicomv
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit - 1;
}

define( 'NICOMV_THEME_URI', get_template_directory_uri() );

if ( ! function_exists( 'nicomv_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function nicomv_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on nicomv.com, use a find and replace
		 * to change 'nicomv' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'nicomv', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'nicomv-featured-image', 2000, 1200, true );
		add_image_size( 'nicomv-thumbnail-avatar', 100, 100, true );
		add_image_size( 'nicomv-post-navigation', 300, 300, true );

		// Setup menus.
		require_once 'inc/menus-setup.php';
		nicomv_setup_menus();

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
			);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
			'nicomv_custom_background_args',
			array(
				'default-color' => 'ffffff',
				'default-image' => '',
			)
			)
			);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		add_theme_support(
			'editor-font-sizes',
			array(
				array(
					'name'      => __( 'Small', 'nicomv' ),
					'shortName' => __( 'S', 'nicomv' ),
					'size'      => 13,
					'slug'      => 'small',
				),
				array(
					'name'      => __( 'Normal', 'nicomv' ),
					'shortName' => __( 'M', 'nicomv' ),
					'size'      => 16,
					'slug'      => 'normal',
				),
				array(
					'name'      => __( 'Large', 'nicomv' ),
					'shortName' => __( 'L', 'nicomv' ),
					'size'      => 28,
					'slug'      => 'large',
				),
				array(
					'name'      => __( 'Huge', 'nicomv' ),
					'shortName' => __( 'XL', 'nicomv' ),
					'size'      => 36,
					'slug'      => 'huge',
				),
			)
		);

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
			);
	}
endif;
add_action( 'after_setup_theme', 'nicomv_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function nicomv_content_width() {
   $GLOBALS['content_width'] = apply_filters( 'nicomv_content_width', 640 );
}
add_action( 'after_setup_theme', 'nicomv_content_width', 0 );

/**
 * Retrieve a list of posts from the Projects category.
 *
 * @return array The list of posts.
 */
function nicomv_blog_posts() {
	$cat_id = get_cat_ID( __( 'Projects', 'nicomv' ) );
	$args = array( 'category__not_in' => array( $cat_id ) );
	return new WP_Query( $args );
}

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function nicomv_widgets_init() {
	register_sidebar(
		[
			'name'          => esc_html__( 'Sidebar', 'nicomv' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here', 'nicomv' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		]
		);

	register_sidebar(
		[
			'name'          => esc_html__( 'Footer - Main Widget Area', 'nicomv' ),
			'id'            => 'footer-main-widget-area',
			'description'   => esc_html__( 'Add widgets to the footer', 'nicomv' ),
			'before_widget' => '<li id="%1$s" class="widget %2$s">',
			'after_widget'  => '</li>',
			'before_title'  => '<h4 class="widget-title is-size-6">',
			'after_title'   => '<div class="separator orange-separator"></div></h4>',
		]
		);
}
add_action( 'widgets_init', 'nicomv_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function nicomv_scripts() {
	wp_enqueue_style( 'lato-font', 'https://fonts.googleapis.com/css?family=Lato', null, '0.0.1' );
	wp_enqueue_style( 'roboto-font', 'https://fonts.googleapis.com/css?family=Roboto', null, '0.0.1' );
	wp_enqueue_style( 'nicomv-theme', get_template_directory_uri() . '/assets/css/theme.css', false, '0.0.3' );

	if ( ! is_single() ) {
		wp_enqueue_script( 'nmv-pg-masonry-setup' );
	}

	wp_enqueue_script( 'nicomv-lib', get_template_directory_uri() . '/assets/js/main.js', array( 'jquery' ), '0.0.1', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'nicomv_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

require get_template_directory() . '/inc/class-nicomv-themecustomizer.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Menu functions.
 */
require get_template_directory() . '/inc/menu-functions.php';
