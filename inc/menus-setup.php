<?php
/**
 * Theme menu setup.
 *
 * @package nicomv
 */

if ( ! function_exists( 'nicomv_setup_menus' ) ) {
	/**
	 * Registers the theme menu locations.
	 */
	function nicomv_setup_menus() {
		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'blog-menu'             => esc_html__( 'Blog Menu', 'nicomv' ),
				'frontpage-menu'        => esc_html__( 'Front page menu', 'nicomv' ),
				'language'              => esc_html__( 'Language Menu', 'nicomv' ),
				'social-menu-header'    => esc_html__( 'Header Social Menu', 'nicomv' ),
				'social-menu-footer'    => esc_html__( 'Footer Social Menu', 'nicomv' ),
				'footer-language-menu'  => esc_html__( 'Footer Language Menu', 'nicomv' ),
			)
			);
	}
}
