<?php
/**
 * Theme customizer options and UI.
 *
 * @package nicomv
 */

/**
 * Helper class to add settings and controls to the WordPress customizer.
 */
class Nicomv_ThemeCustomizer {

	/**
	 * The sections of the theme customization.
	 *
	 * @var Array
	 */
	private $sections;

	/**
	 * An instance of the WordPress WP_Customize_Manager.
	 *
	 * @var WP_Customize_Manager
	 */
	private $customizer;

	/**
	 * Builds an instance of this class.
	 *
	 * @param WP_Customize_Manager $customizer WordPress customize manager instance.
	 */
	public function __construct( WP_Customize_Manager $customizer ) {
		$this->customizer = $customizer;
		$this->sections = array(
			'nicomv_themesettings' => array(
				'section' => array(
					'title' => __( 'Theme specific settings', 'nicomv' ),
					'priority' => 30,
				),
				'settings' => array(),
			),
		);

		$this->add_sections();
		$this->add_sticky_menu();
		$this->add_navigation_search_bar();
		$this->add_comments_option();
		$this->register();
	}

	/**
	 * Adds the settings, controls and sections previously enqueued.
	 */
	private function register() {
		foreach ( $this->sections as $section_name => $section ) {
			foreach ( $section['settings'] as $index => $setting ) {
				$name = $setting['name'];
				$this->customizer->add_setting(
					$name,
					$setting['args']
				);

				$control_id = $name . '_control';
				$setting['control']['settings'] = $name;
				$setting['control']['section'] = $section_name;
				$this->customizer->add_control(
					$control_id,
					$setting['control']
				);
			}
		}
	}

	/**
	 * Registers the sections.
	 */
	private function add_sections() {
		foreach ( $this->sections as $key => $section ) {
			if ( isset( $section['section'] ) ) {
				$this->customizer->add_section( $key, $section['section'] );
			}
		}
	}

	/**
	 * Adds the sticky menu section.
	 */
	private function add_sticky_menu() {
		$setting = array(
			'name' => 'nicomv_stickymenu',
			'args' => array(
				'default' => '1',
				'transport' => 'refresh',
			),
			'control' => array(
				'label' => __( 'Sticky main navigation', 'nicomv' ),
				'description' => __( 'Used to specify if the main navigation should stick to the top', 'nicomv' ),
				'type' => 'checkbox',
				'choices' => array(
					'1'   => __( 'Yes', 'nicomv' ),
					'0'    => __( 'No', 'nicomv' ),
				),
			),
		);
		$this->sections['nicomv_themesettings']['settings'][] = $setting;
	}

	/**
	 * Main navigation search form.
	 */
	private function add_navigation_search_bar() {
		$setting = array(
			'name' => 'nicomv_navsearchbar',
			'args' => array(
				'default' => '1',
				'transport' => 'refresh',
			),
			'control' => array(
				'label' => __( 'Navigation Search Bar', 'nicomv' ),
				'description' => __( 'Specifies if a search form should be attached to the main navigation', 'nicomv' ),
				'type' => 'checkbox',
				'choices' => array(
					'1'   => __( 'Yes', 'nicomv' ),
					'0'    => __( 'No', 'nicomv' ),
				),
			),
		);
		$this->sections['nicomv_themesettings']['settings'][] = $setting;
	}

	/**
	 * Add the comments toggle option.
	 */
	private function add_comments_option() {
		$setting = array(
			'name' => 'nicomv_disablecomments',
			'args' => array(
				'default' => '1',
				'transport' => 'refresh',
			),
			'control' => array(
				'label' => __( 'Disable Comments', 'nicomv' ),
				'description' => __( 'Disable comments globally', 'nicomv' ),
				'type' => 'checkbox',
				'choices' => array(
					'1' => __( 'Yes', 'nicomv' ),
					'0' => __( 'No', 'nicomv' ),
				),
			),
		);
		$this->sections['nicomv_themesettings']['settings'][] = $setting;
	}
}
