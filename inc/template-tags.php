<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package nicomv
 */

if ( ! function_exists( 'nicomv_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function nicomv_posted_on() {
		get_template_part( 'template-parts/meta', 'posted-on' );
	}
endif;

if ( ! function_exists( 'nicomv_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function nicomv_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			nicomv_post_cats();
			nicomv_post_tags();
		}

		$are_comments_disabled = nicomv_are_comments_disabled();

		if ( $are_comments_disabled ) {
			return;
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link(
				sprintf(
					wp_kses(
						/* translators: %s: post title */
						__( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'nicomv' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				)
			);
			echo '</span>';
		}
	}
endif;

if ( ! function_exists( 'nicomv_edit_link' ) ) {
	/**
	 * Taken from Twenty Seventeen theme (inc/template-tags.php).
	 */
	function nicomv_edit_link() {
		if ( ! is_user_logged_in() ) {
			return;
		}
		if ( ! current_user_can( 'edit_posts' ) ) {
			return;
		}
		set_query_var(
			'edit_link_args',
			array(
				'text' => __( 'Edit Entry', 'nicomv' ),
				'title' => get_the_title(),
				'url'   => get_edit_post_link(),
			)
			);
		get_template_part( 'template-parts/edition', 'edit-link' );
	}
}

if ( ! function_exists( 'nicomv_post_tags' ) ) {
	/**
	 * Get the current post tags template.
	 */
	function nicomv_post_tags() {
		$tags = get_the_tags();
		if ( ! $tags ) {
			return;
		}
		$_tags = array();
		foreach ( $tags as $tag ) {
			$_tags[] = array(
				'tag_name'  => $tag->name,
				'tag_url'   => get_tag_link( $tag->term_id ),
				// translators: %s is the tag name.
				'tag_title' => sprintf( __( 'Tag %s', 'nicomv' ), $tag->name ),
				'classes'   => $tag->slug,
			);
		}
		set_query_var( 'nmv_post_tags', $_tags );
		get_template_part( 'template-parts/content', 'tags' );
	}
}

if ( ! function_exists( 'nicomv_post_cats' ) ) {
	/**
	 * Get the current post categories template.
	 */
	function nicomv_post_cats() {
		get_template_part( 'template-parts/meta', 'category' );
	}
}

if ( ! function_exists( 'nicomv_post_navigation' ) ) {
	/**
	 * Get the current post navigation template.
	 */
	function nicomv_post_navigation() {
		$wp_terms = get_the_category();
		$cats = '';

		foreach ( $wp_terms as $term ) {
			$cats .= "$term->cat_ID,";
		}

		$cats = trim( $cats, ',' );
		$query_args = [
			'cat' => $cats,
			'orderby' => 'rand',
			'post__not_in' => [ get_the_ID() ],
			'posts_per_page' => 4,
		];
		$query = new WP_Query( $query_args );

		if ( ! $query->have_posts() ) {
			wp_reset_postdata();
			return;
		}

		set_query_var(
			'nmv_pn',
			$query
		);
		get_template_part( 'template-parts/post', 'navigation' );
		wp_reset_postdata();
	}
}
