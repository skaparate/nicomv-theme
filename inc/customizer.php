<?php
/**
 * Theme Customizer.
 *
 * @package nicomv
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function nicomv_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		error_log( 'Selective refresh is active' );
		$wp_customize->selective_refresh->add_partial(
			 'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'nicomv_customize_partial_blogname',
			)
			);
		$wp_customize->selective_refresh->add_partial(
			 'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'nicomv_customize_partial_blogdescription',
			)
			);
	}

	$theme_settings = array(
		'section' => array(
			'title' => __( 'Theme specific settings', 'nicomv' ),
			'priority' => 30,
		),
		'setting_name' => 'nicomv_stickymenu',
		'setting_args' => array(
			'transport' => 'refresh',
			'default' => 'yes',
		),
		'control' => array(
			'label' => __( 'Sticky main navigation', 'nicomv' ),
			'description' => __( 'Used to specify if the main navigation should stick to the top', 'nicomv' ),
			'type' => 'checkbox',
			'choices' => array(
				'yes'   => __( 'Yes', 'nicomv' ),
				'no'    => __( 'No', 'nicomv' ),
			),
		),
	);

	$helper = new Nicomv_ThemeCustomizer( $wp_customize );
}
add_action( 'customize_register', 'nicomv_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function nicomv_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function nicomv_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function nicomv_customize_preview_js() {
	wp_enqueue_script( 'nicomv-customizer', get_template_directory_uri() . '/js/lib/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'nicomv_customize_preview_js' );
