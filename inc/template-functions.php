<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package nicomv
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function nicomv_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'nicomv_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function nicomv_pingback_header() {
	 if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'nicomv_pingback_header' );

/**
 * Retrieve the comment form template.
 *
 * @param array $defaults The default values.
 */
function nicomv_comment_form( $defaults ) {
	$defaults['submit_button'] = '<button name="%1$s" type="submit" id="%2$s" class="%3$s"><span class="icon-send"></span>%4$s</button>';
	return $defaults;
}
add_filter( 'comment_form_defaults', 'nicomv_comment_form', 10, 1 );

/**
 * Retrieve a template part.
 *
 * @param string $template The template path.
 * @param string $part The template part.
 */
function nicomv_get_template_part( $template, $part ) {
	include locate_template( $template . '-' . $part . '.php' );
}

/**
 * Removes the versions on the urls.
 *
 * @param string $src The src url.
 * @param string $handle The handler.
 */
function nicomv_src_cleaner( $src, $handle ) {
	$qpos = strrpos( $src, '?' );
	if ( false === $qpos ) {
		return $src;
	}
	$src = substr( $src, 0, $qpos );
	return $src;
}

/**
 * Checks if the sticky menu option is set.
 */
function nicomv_is_sticky_menu() {
	return get_theme_mod( 'nicomv_stickymenu' );
}

/**
 * Checks if the search bar should be added to the main navigation bar.
 */
function nicomv_use_navigation_search_bar() {
	return get_theme_mod( 'nicomv_navsearchbar' );
}

/**
 * Checks if the comments are enabled or disabled on the theme.
 */
function nicomv_are_comments_disabled() {
	return get_theme_mod( 'nicomv_disablecomments', true );
}
