<?php
/**
 * Functions for menus.
 *
 * @package nicomv
 */

// phpcs:disable Squiz.Commenting.FunctionComment.MissingParamTag
/**
 * Adds the specific class to all menu items.
 *
 * @see https://codex.wordpress.org/Plugin_API/Filter_Reference/nav_menu_css_class
 * @return string The menu classes.
 */
function nicomv_custom_menu_item( $classes, $item, $args, $depth ) {
	$classes[] = 'navbar-item';
	return $classes;
}
// phpcs:enable

add_filter( 'nav_menu_css_class', 'nicomv_custom_menu_item', 10, 4 );

require get_template_directory() . '/inc/class-nicomv-bulmamenuwalker.php';
require get_template_directory() . '/inc/class-nicomv-socialmenuwalker.php';
