<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nicomv
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer footer">
		<?php
			if ( ! is_user_logged_in() ) :
			get_template_part( 'template-parts/content', 'cookie-warning' );
			endif;
		?>
		<?php
			get_template_part( 'template-parts/footer', 'front-page' );
		?>
			<div class="columns is-vcentered">
			<?php if ( has_nav_menu( 'social-menu-footer' ) ) : ?>
				<div class="column is-flex-mobile">
					<h4 class="title is-size-6"><?php echo esc_html__( 'Social', 'nicomv' ); ?></h4>
					<hr class="separator orange-separator" />
					<nav class="navbar navigation footer-menu">
						<?php
						$_menu_options = array(
							'theme_location'    => 'social-menu-footer',
							'menu_id'           => 'social-menu-footer',
							'container'         => null,
							'walker'            => new Nicomv_SocialMenuWalker(),
							'items_wrap'        => '%3$s',
						);
						wp_nav_menu( $_menu_options );
						?>
					</nav>
				</div>
				<?php endif; ?>
				<div class="column is-8 has-text-centered">
					<p>
						<i class="icon-copyright"></i>
						<?php
							// translators: %s is the current year.
							echo esc_html( sprintf( __( 'All rights reserved %s', 'nicomv' ), date( 'Y' ) ) );
						?>
						<strong title="Nicolas Mancilla Vergara">NicoMV</strong>
					</p>
				</div>
				<?php if ( is_active_sidebar( 'footer-main-widget-area' ) ) : ?>
				<ul id="footer-main-widget-area" class="column">
					<?php dynamic_sidebar( 'footer-main-widget-area' ); ?>
				</ul>
				<?php endif; ?>
				<?php if ( has_nav_menu( 'footer-language-menu' ) ) : ?>
				<div class="column is-flex-mobile">
					<h4 class="title is-size-6"><?php echo esc_html__( 'Language', 'nicomv' ); ?></h4>
					<hr class="separator orange-separator" />
					<nav class="navbar navigation footer-menu">
						<?php
						$_menu_options = array(
							'theme_location'    => 'footer-language-menu',
							'menu_id'           => 'footer-language-menu',
							'container'         => null,
							'walker'            => new Nicomv_BulmaMenuWalker(),
							'items_wrap'        => '%3$s',
						);
						wp_nav_menu( $_menu_options );
						?>
					</nav>
				</div>
				<?php endif; ?>
			</div>
	</footer><!-- #colophon -->
</div><!-- #page -->
<?php $_top = __( 'Go to the Top', 'nicomv' ); ?>
<a href="#top" id="go-top" class="ps2id" title="<?php echo esc_html( $_top ); ?>">
	<span class="sr-only"><?php esc_html( $_top ); ?></span>
	<i class="icon-up-open"></i>
</a>
<?php wp_footer(); ?>

</body>
</html>
