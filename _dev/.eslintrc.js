module.exports = {
  extends: ['airbnb', 'prettier'],
  env: {
    browser: true,
    node: true,
  },
  rules: {
    'import/extensions': 0,
    'no-console': 0,
    'spaced-comment': 0
  },
};
