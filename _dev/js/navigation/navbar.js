const $ = require('jquery');

/**
 * Toggles the icon on the burger button.
 *
 * @param {jQuery} $button The burger button.
 */
function toggleIconOnButton($button) {
  if ($button.classList.contains('icon-menu')) {
    $button.classList.replace('icon-menu', 'icon-cancel');
  } else {
    $button.classList.replace('icon-cancel', 'icon-menu');
  }
}

function closeMenu() {
  const burgerButton = document.querySelector(
    '.main-navigation .navbar-burger'
  );

  // Closes the menu when a link is clicked.
  if (burgerButton.classList.contains('is-active')) {
    burgerButton.click();
  }
}

/**
 * Handles the menu item click.
 */
function onMenuItemClick() {
  closeMenu();
}

function navBurgerClick(event) {
  let e;
  const nodeName = event.target.nodeName.toLowerCase();
  if (nodeName === 'span' || nodeName === 'i') {
    e = event.target.parentNode;
  } else {
    e = event.target;
  }
  const $el = $(e)[0];
  const {
    dataset: { target },
  } = $el;
  const $target = $(`#${target}`);

  // Toggle the class on both the "navbar-burger" and the "navbar-menu"
  if ($target.hasClass('is-active')) {
    $target.fadeOut(500, () => {
      $target.removeClass('is-active');
    });
  } else {
    $target.fadeIn(500, () => {
      $target.addClass('is-active');
    });
  }

  $el.classList.toggle('is-active');
  $target.parent().toggleClass('is-active');
  toggleIconOnButton($el.lastElementChild);
}

function navMenuIterator(el) {
  el.addEventListener('click', navBurgerClick);
}

function onScroll() {
  const $masthead = $('#masthead');
  const $body = $('body');
  const $window = $(window);
  const width = $window.outerWidth();
  const $adminBar = $('#wpadminbar');

  if ($body.hasClass('fixed-top-nav') && $body.hasClass('admin-bar')) {
    const scroll = $window.scrollTop();
    const adminBarH = $adminBar.outerHeight();

    if (width <= 600) {
      let topPx = adminBarH - scroll;
      if (topPx <= 0) {
        topPx = 0;
      }
      $masthead.css('top', topPx);
    } else {
      $masthead.css('top', adminBarH);
    }
  }
}

function onWindowResize() {
  $(window).on('scroll', onScroll);
}

function documentReady() {
  const $navbarBurgers = Array.prototype.slice.call(
    document.querySelectorAll('.navbar-burger'),
    0
  );
  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {
    // Add a click event on each of them
    $navbarBurgers.forEach(navMenuIterator);
  }

  $('.main-navigation .navbar-center a').on('click', onMenuItemClick);

  $(window).resize(onWindowResize);
  $(window).resize();
}

$(document).ready(documentReady);
