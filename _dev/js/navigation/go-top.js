const $ = require('jquery');

function onGoToTopClick() {
  const burgerMenuButton = $('.main-navigation .navbar-burger');

  if (burgerMenuButton.hasClass('is-active')) {
    burgerMenuButton.click();
  }
}

function onWindowScroll() {
  const $window = $(window);
  const h = $window.innerHeight() / 4;
  const st = $window.scrollTop();
  const $gotop = $('#go-top');
  if (st >= h) {
    $gotop.fadeIn();
    $gotop.on('click', onGoToTopClick);
  } else {
    $gotop.fadeOut();
    $gotop.off('click');
  }
}

$(document).ready(() => {
  $(window).on('resize', () => {
    $(window).on('scroll', onWindowScroll);
    $(window).scroll();
  });
  $(window).resize();
});
