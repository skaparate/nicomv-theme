const $ = require('jquery');

const keyEvent = 'keydown';

function handleSpecialKeys(event) {
  const key = event.originalEvent.keyCode;
  if (key === 27) {
    event.data.t.trigger('click');
  }
}

function attachKeyHandler($text, data) {
  $text.on(keyEvent, data, handleSpecialKeys);
}

function detachKeyHandler($text) {
  $text.off(keyEvent, handleSpecialKeys);
}

function hideForm($form, $el) {
  console.log('Hiding form');
  $form.removeClass('is-open');
  detachKeyHandler($form.find('input.search'));
  $form.slideUp();
  $el.find('.icon-cancel').removeClass('icon-cancel').addClass('icon-search');
}

function showForm($form, $trigger) {
  console.log('Showing form');
  $form.addClass('is-open');
  $trigger.find('.icon-search').removeClass('icon-search').addClass('icon-cancel');

  function animationFinished() {
    attachKeyHandler($form.find('input.search'), {
      f: $form,
      t: $trigger,
      s: $form.find('.search-submit')
    });
  }
  $form.slideDown(600, animationFinished);
}

function onSearchToggleClick(event) {
  event.preventDefault();
  console.log('clicked', event);
  const $el = $(event.currentTarget);
  const $form = $el.next('form');
  console.log('Element:', $el);
  console.log('Form:', $form);
  if ($form.hasClass('is-open')) {
    hideForm($form, $el);
  } else {
    showForm($form, $el);
  }
}

function documentReady() {
  $('.search-toggle').on({
    click: onSearchToggleClick
  });
}

$(document).ready(documentReady);
