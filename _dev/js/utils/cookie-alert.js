import Cookies from 'js-cookie';

const $ = require('jquery');

function NmvCookieAcceptance() {
  const cookieName = 'cookie_law_acceptance';
  return {
    hasAcceptedCookie() {
      return Cookies.get(cookieName);
    },
    handleAcceptClick(event) {
      event.stopPropagation();
      Cookies.set(cookieName, 'true', {
        expires: 365
      });
      const $parent = $(event.target).parent();
      $parent.fadeOut(() => {
        $parent.remove();
      });
    }
  };
}

function onDocumentReady() {
  const ca = new NmvCookieAcceptance();
  if(ca.hasAcceptedCookie()) return;
  const $cwarning = $('.cookie-warning');
  $cwarning.css('display', 'flex')
  .hide()
  .fadeIn(1000);
  $cwarning.find('.accept').on('click', ca.handleAcceptClick);
}

$(document).ready(onDocumentReady);
