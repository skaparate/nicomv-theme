function NmvLocalStorage() {
  return {
    getItem(sKey) {
      if (!sKey || !Object.prototye.hasOwnProperty.call(this, sKey)) { return null; }
      const escapedKey = escape(sKey).replace(/[-.+*]/g, "\\$&");
      const regexPrefix = "(?:^|.*;\\s*)";
      const regexSuffix = "\\s*\\=\\s*((?:[^;](?!;))*[^;]?).*";
      return unescape(document.cookie.replace(new RegExp(`${regexPrefix}${escapedKey}${regexSuffix}`), "$1"));
    },
    key(nKeyId) {
      return unescape(document.cookie.replace(/\s*=(?:.(?!;))*$/, "").split(/\s*=(?:[^;](?!;))*[^;]?;\s*/)[nKeyId]);
    },
    setItem(sKey, sValue) {
      if(!sKey) { return; }
      document.cookie = `${escape(sKey)}=${escape(sValue)}; expires=Tue, 19 Jan 2038 03:14:07 GMT; path=/`;
      this.length = document.cookie.match(/=/g).length;
    },
    length: 0,
    removeItem(sKey) {
      if (!sKey || !Object.prototye.hasOwnProperty.call(this, sKey)) { return; }
      document.cookie = `${escape(sKey)}=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/`;
      this.length -= 1;
    },
    hasOwnProperty(sKey) {
      const escapedKey = escape(sKey).replace(/[-.+*]/g, "\\$&");
      const regexPrefix = "(?:^|;\\s*)";
      const regexSuffix = "\\s*\\=";
      return (new RegExp(`${regexPrefix}${escapedKey}${regexSuffix}`)).test(document.cookie);
    }
  };
}

if (!window.localStorage) {
  window.localStorage = new NmvLocalStorage();
  window.localStorage.length = (document.cookie.match(/=/g) || window.localStorage).length;
}
