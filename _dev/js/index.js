import 'jquery';
import 'js-cookie';
import '../sass/theme.scss';
import './utils/index.js';
import './navigation/index';
import './layout/index';
