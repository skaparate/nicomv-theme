const webpack = require('webpack');
const path = require('path');
const CleanPlugin = require('clean-webpack-plugin');
const commonConfig = require('./common.config.js');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const extractSCSS = new ExtractTextPlugin('../css/theme.css');
const jsInputPath = path.resolve(commonConfig.baseInputPath, 'js');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = {
  entry: {
    main: path.resolve(jsInputPath, 'index.js')
  },
  output: {
    path: path.resolve(commonConfig.baseOutputPath, 'js'),
    filename: '[name].js'
  },
  resolve: {
    extensions: ['.js', '.scss', '.css']
  },
  module: {
    rules: [{
        test: /\.js$/,
        exclude: ['/node_modules/'],
        use: 'eslint-loader',
        enforce: 'pre'
      },
      {
        test: /\.js$/,
        exclude: [/(node_modules|bower_components)(?![/|\\](bootstrap|foundation-sites))/],
        use: 'babel-loader'
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                minimize: true || { },
                config: {
                  path: path.resolve(commonConfig.baseInputPath, 'config/postcss.config.js')
                }
              }
            },
            {
              loader: 'sass-loader'
            }
          ]
        })
      },
      // Static files
      {
        test: /\.(ttf|woff2?|eot|png|jpe?g|gif|svg|ico)$/i,
        use: 'url-loader'
      }
    ]
  },
  plugins: [
    new CleanPlugin([commonConfig.baseOutputPath], {
      root: path.resolve(commonConfig.baseOutputPath, '..')
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV)
      }
    }),
    new CopyWebpackPlugin([
      {
        from: path.join(commonConfig.baseInputPath, '/js/lib/**/*'),
        to: path.resolve(commonConfig.baseOutputPath)
      }
    ]),
    new webpack.ProvidePlugin({
      "window.jQuery": "jquery",
      "window.$": "jquery",
      "jQuery": "jquery",
      "$": "jquery"
    }),
    new webpack.NoEmitOnErrorsPlugin(),
    extractSCSS,
    new webpack.optimize.UglifyJsPlugin()
    //new UglifyJsPlugin()
  ]
};

// module.exports = config;
