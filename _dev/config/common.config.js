const path = require('path');

const basePath = path.resolve(__dirname, '..');

const config = {
  baseOutputPath: path.resolve(basePath, '..', 'assets'),
  baseInputPath: basePath,
  basePath: path.resolve(basePath, '..')
};

module.exports = config;
