<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package nicomv
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="error-404 not-found section">
				<header class="page-header columns is-mobile">
					<h1 class="page-title column is-12 error-404--title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'nicomv' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content columns is-mobile">
					<div class="column is-6 is-offset-3">
						<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'nicomv' ); ?></p>
						<?php get_search_form(); ?>
					</div>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
