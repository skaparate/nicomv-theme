<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package nicomv
 */

?>
<section class="no-results not-found">
	<header class="page-header columns is-vcentered">
		<h2 class="page-title column is-8 is-offset-1"><?php esc_html_e( 'Nothing Found', 'nicomv' ); ?></h2>
	</header><!-- .page-header -->

	<div class="page-content">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) :
		?>
			<p>
			<?php
				printf(
					wp_kses(
						/* translators: 1: link to WP admin new post page. */
						__( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'nicomv' ),
						array(
							'a' => array(
								'href' => array(),
							),
						)
					),
					esc_url( admin_url( 'post-new.php' ) )
				);
			?>
			</p>
		<?php
		endif;
		if ( is_search() ) :
		?>
		<div class="columns is-vcentered">
			<div class="column is-offset-1 is-4">
			<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'nicomv' ); ?></p>
			<?php
			get_search_form();
			?>
			</div><!-- .column -->
			<div class="column is-2">

			</div><!-- .column -->
		</div><!-- .columns -->
		<?php
		else :
		?>
		<div class="columns is-vcentered">
			<div class="column is-offset-1 is-4">
			<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'nicomv' ); ?></p>
			</div><!-- .column -->
			<div class="column is-2">
			<?php
			get_search_form();
			?>
			</div><!-- .column -->
		</div><!-- .columns -->
		<?php
		endif;
		?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
