<?php
/**
 * Template for the categories of a post.
 *
 * @package nicomv
 */

$cats = get_the_category_list( ', ' );
if ( $cats ) :
?>
<div class="categories">
	<i class="icon-tag" title="<?php echo esc_html_e( 'Categories this post is published on', 'nicomv' ); ?>"></i>
	<?php echo ( $cats ); ?>
</div>
<?php
endif;
?>
