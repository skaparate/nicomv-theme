<?php
/**
 * Content tags template.
 *
 * @package nicomv
 */

$_tags = get_query_var( 'nmv_post_tags' );

?>
<div class="tags">
	<i class="icon-tags" title="<?php esc_html_e( 'Tags', 'nicomv' ); ?>"></i>
	<?php foreach ( $_tags as $_tag ) : ?>
		<a href="<?php echo esc_html( $_tag['tag_url'] ); ?>" title="<?php echo esc_html( $_tag['tag_title'] ); ?>" class="tag <?php echo esc_html( $_tag['classes'] ); ?>">
			<?php echo esc_html( $_tag['tag_name'] ); ?>
		</a>
	<?php endforeach; ?>
</div>
