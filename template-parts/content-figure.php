<?php
/**
 * Template for an image figure.
 *
 * @package nicomv
 */

if ( has_post_thumbnail() ) :
?>
<figure>
	<?php
	$_post_thumb_id = get_post_thumbnail_id( $post->ID );
	$_post_thumb_src = wp_get_attachment_image_src( $_post_thumb_id, 'full' );
	$_post_thumb_alt = get_post_meta( $_post_thumb_id, '_wp_attachment_image_alt', true );
	?>
	<img src="<?php echo esc_html( $_post_thumb_src[0] ); ?>" alt="<?php echo esc_html( $_post_thumb_alt ); ?>" width="<?php echo esc_html( $_post_thumb_src[1] ); ?>" height="<?php echo esc_html( $_post_thumb_src[2] ); ?>">
</figure>
<?php endif; ?>
