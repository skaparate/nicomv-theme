<?php
/**
 * Template of a single recommended post.
 *
 * @package nicomv
 */

	$nmv_pn_post = get_query_var( 'nmv_pn_post' );
	$_post_id = get_the_ID();
	$_permalink = get_permalink( $_post_id );
	$_has_image = has_post_thumbnail( $_post_id );
?>
<div id="post-<?php echo esc_html( $_post_id ); ?>" class="<?php echo esc_html( $nmv_pn_post['class'] ); ?><?php echo $_has_image ? '' : ' no-image'; ?>">
	<?php
	if ( $_has_image ) :
		$_post_thumb_id = get_post_thumbnail_id( $_post_id );
		$_post_thumb_src = wp_get_attachment_image_src( $_post_thumb_id, 'full' );
		$_post_thumb_alt = get_post_meta( $_post_thumb_id, '_wp_attachment_image_alt', true );
	?>
	<a href="<?php echo esc_html( $_permalink ); ?>">
		<figure class="post-navigation--figure">
			<img src="<?php echo esc_html( $_post_thumb_src[0] ); ?>" alt="<?php echo esc_html( $_post_thumb_alt ); ?>" width="<?php echo esc_html( $_post_thumb_src[1] ); ?>" height="<?php echo esc_html( $_post_thumb_src[2] ); ?>">
		</figure>
	</a>
	<?php endif; ?>
	<div class="entry-header post-navigation--header">
		<a href="<?php echo esc_html( $_permalink ); ?>">
			<h3 class="entry-title post-navigation--header-title">
				<?php the_title(); ?>
			</h3><!-- .post-navigation--header-title -->
		</a>
	</div><!-- .post-navigation--header -->
</div><!-- <?php echo esc_html( $nmv_pn_post['class'] ); ?> -->
