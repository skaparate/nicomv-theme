<?php
/**
 * Template for the edit post link.
 *
 * @package nicomv
 */

$args = get_query_var( 'edit_link_args' );
?>
<div class="edit-link">
	<a href="<?php echo esc_html( $args['url'] ); ?>" title="<?php echo esc_html( $args['text'] ); ?> '<?php echo esc_html( $args['title'] ); ?>'">
		<span class="sr-only"><?php echo esc_html( $args['text'] ); ?> "<?php echo esc_html( $args['title'] ); ?>"</span>
		<i class="icon-pencil"></i>
	</a>
</div>
