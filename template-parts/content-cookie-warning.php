<?php
/**
 * Cookie policy warning template.
 *
 * @package nicomv
 */

?>
<div class="notification cookie-warning is-warning" role="alert">
	<?php esc_html_e( 'Like many sites, we too use cookies to enhance the user experience.', 'nicomv' ); ?>
	<button class="button accept" type="button">
		<?php esc_html_e( 'Accept', 'nicomv' ); ?>
	</button>
</div>
