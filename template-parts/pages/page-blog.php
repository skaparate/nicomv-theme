<?php
/**
 * Displays content for blog page.
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
	<div class="panel-content">
		<?php $_the_title = get_the_title(); ?>
		<div class="wrap">
			<header class="entry-header is-clearfix">
				<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php echo esc_html( $_the_title ); ?>"><?php echo esc_html( $_the_title ); ?></a></h2>
				<?php nicomv_posted_on(); ?>
				<?php nicomv_edit_link( get_the_ID() ); ?>
			</header><!-- .entry-header -->
			<div class="orange-separator separator"></div>
			<div class="entry-figure">
				<?php get_template_part( 'template-parts/content', 'figure' ); ?>
			</div>

			<div class="entry-content">
				<?php
					/* translators: %s: Name of current post */
					the_content( sprintf( __( 'Continue reading "%s"', 'nicomv' ), $_the_title ) );
				?>
			</div><!-- .entry-content -->
			<div class="entry-footer">
				<?php nicomv_post_tags(); ?>
			</div><!-- .entry-footer -->
		</div><!-- .wrap -->
	</div><!-- .panel-content -->

</article><!-- #post-## -->
