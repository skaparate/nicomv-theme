<?php
/**
 * Template for the post meta update and creation date.
 *
 * @package nicomv
 */

	$_posted_date = get_the_date();
	$_updated_date = get_the_modified_date();
?>
<div class="entry-date">
	<i class="icon-calendar"></i>
	<time class="published" datetime="<?php echo get_the_date( 'c' ); ?>" title="<?php esc_html_e( 'Published on', 'nicomv' ); ?>">
		<?php echo esc_html( $_posted_date ); ?>
	</time>
	<?php
	if ( $_posted_date !== $_updated_date ) :
	?>
		/ <time class="updated" datetime="<?php echo esc_html( get_the_modified_date( 'c' ) ); ?>">
			<?php esc_html_e( 'Last update:', 'nicomv' ); ?> <?php echo esc_html( $_updated_date ); ?>
		</time>
	<?php endif; ?>
</div>
