<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package nicomv
 */

$_extra_class = has_category( __( 'Projects', 'nicomv' ) ) && ! is_archive() ? ' single-project' : '';
$_extra_class .= ' white';
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( $_extra_class ); ?>>
	<div class="entry-figure">
		<?php if ( is_singular() ) : ?>
		<?php get_template_part( 'template-parts/content', 'figure' ); ?>
		<?php else : ?>
		<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
			<?php get_template_part( 'template-parts/content', 'figure' ); ?>
		</a>
		<?php endif; ?>
	</div>
	<header class="entry-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) :
			nicomv_posted_on();
		endif;
		nicomv_edit_link();
		?>
	</header><!-- .entry-header -->
	<?php
		if ( '' !== $post->post_content ) :
			?>
	<div class="entry-content">
		<?php
			the_content(
				sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading <span class="screen-reader-text">"%s"</span>', 'nicomv' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			)
				);
			wp_link_pages(
				 array(
					 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'nicomv' ),
					 'after'  => '</div>',
				 )
				);
		?>
	</div><!-- .entry-content -->
			<?php endif; ?>
	<footer class="entry-footer">
		<?php nicomv_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
