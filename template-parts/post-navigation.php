<?php
/**
 * Posts shown under a single post.
 *
 * @package nicomv
 */

	$nmv_pn = get_query_var( 'nmv_pn' );
	$_query = $nmv_pn;
	$_classes = 'column is-3 post-navigation--item';
?>
<div class="navigation post-navigation">
	<h3 class="navigation-title post-navigation--title"><?php esc_html_e( 'Recommended', 'nicomv' ); ?></h3>
	<div class="columns">
		<?php
		while ( $_query->have_posts() ) :
			$_query->the_post();
			set_query_var(
				 'nmv_pn_post',
				[
					'class' => $_classes,
				]
				);
			get_template_part( 'template-parts/post-navigation', 'entry' );
		?>
		<?php endwhile; ?>
	</div><!-- .columns -->
</div><!-- .post-navigation -->
