<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package nicomv
 */

get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			if ( have_posts() ) :
			?>
				<header class="page-header">
					<div class="columns is-vcentered is-mobile">
						<div class="column is-offset-1 is-7 is-three-quarters-mobile">
							<?php
								the_archive_title( '<h2 class="page-title">', '</h2>' );
								the_archive_description( '<div class="archive-description">', '</div>' );
							?>
						</div>

						<?php if ( function_exists( 'yoast_breadcrumb' ) ) : ?>
						<div class="column">
							<?php yoast_breadcrumb( '<div class="yoast-breadcrumb">', '</div>' ); ?>
						</div>
						<?php endif; ?>
					</div>
				</header><!-- .page-header -->
				<div class="columns">
					<div class="column is-offset-1 is-7-tablet is-full-mobile">
						<div class="grid">
				<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();
					?>
					<div class="grid-item">
					<?php

					/*
					* Include the Post-Format-specific template for the content.
					* If you want to override this in a child theme, then include a file
					* called content-___.php (where ___ is the Post Format name) and that will be used instead.
					*/
					get_template_part( 'template-parts/content', get_post_format() );
					?>
					</div><!-- .column -->
					<?php endwhile; ?>
					</div><!-- .columns -->
				<?php
				the_posts_navigation();
			else :

					get_template_part( 'template-parts/content', 'none' );

			endif;
			?>
				</div><!-- .column -->
				<div class="column is-3-tablet is-full-mobile">
					<?php get_sidebar(); ?>
				</div>
			</div><!-- .columns -->
		</main><!-- #main -->
	</div><!-- #primary -->
</div>
<?php
get_footer();
